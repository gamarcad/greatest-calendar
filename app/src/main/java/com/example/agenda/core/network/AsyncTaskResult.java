package com.example.agenda.core.network;

public class AsyncTaskResult< T > {
    private T result;
    private Exception error;

    public AsyncTaskResult( T result ) {
        super();
        this.result = result;
    }

    public AsyncTaskResult( Exception error ) {
        super();
        this.error = error;
    }

    public boolean hasSuccessed() {
        return !this.hasFailed();
    }

    public boolean hasFailed() {
        return this.error != null;
    }


    public T getResult() {
        return result;
    }

    public Exception getError() {
        return error;
    }
}


