package com.example.agenda.core.storage.Dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;

import java.util.List;

/**
 * ModuleDao is an interface which implement basics queries for work with
 * Module's table
 **/
@Dao
public interface ModuleDao {

    /**
     * Give an Module based on his id
     * @param moduleId the module's id searched
     * @return the module asked
     */
    @Query("SELECT * FROM Module WHERE id = :moduleId")
    Module getModule(long moduleId);

    /**
     * Give all events linked with a module
     * @param moduleId the module's id
     * @return List<Event> an events list
     */
    @Query("SELECT * FROM Event WHERE module_id = :moduleId")
    List<Event> getModuleAllEvent(long moduleId);

    /**
     * Give an Module based on his title
     * @param moduleTitle the module"s tile searched
     * @return the module asked
     */
    @Query("SELECT * FROM Module WHERE title = :moduleTitle")
    Module getModule(String moduleTitle);

    // I can also use LiveData, it's even recommended but I don't understand why...
    /**
     * Give all modules stocked in the table
     * @return An modules list
     */
    @Query("SELECT * FROM Module")
    List<Module> getAllModule();

    /**
     * Insert an Module
     * @param module the module which will be inserted
     * @return module's id
     */
    @Insert
    long insert(Module module);

    /**
     * Update the module chosen
     * @param module the module which will be updated
     * @return I don't know ...
     */
    @Update
    int update(Module module);

    /**
     * Delete an module chosen
     * @param moduleId the module's id which will be deleted
     * @return 1 if it's succeed 0 else I think
     */
    @Query("DELETE FROM Module WHERE id = :moduleId")
    int delete(long moduleId);




}

