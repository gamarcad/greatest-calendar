package com.example.agenda.core.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import com.example.agenda.activities.scholar_years.display.OnItemInteraction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.SortedSet;

/**
 * List adapter wraps simple management items.
 * This adapter can be manipulated as a list.
 *
 * @param <T>
 */
public abstract class ListAdapter<T> extends RecyclerView.Adapter<GenericViewHolder>
        implements List<T> {

    /**
     * Marks -1 as not found index.
     */
    private static final int ITEM_NOT_FOUND_INDEX = -1;

    /**
     * List of items.
     */
    protected List<T> items;

    /**
     * context where adapter is displayed.
     */
    private Context context;

    /**
     * Observers of item interactions.
     */
    private List<OnItemInteraction> observers;

    /**
     * Default constructor
     */
    public ListAdapter() {
        this( new ArrayList<>() );
    }

    /**
     * Initialize adapter with an initial set of items.
     *
     * @param initialList Initial list of items.
     */
    public ListAdapter( List<T> initialList ) {
        this.observers = new ArrayList<>();
        this.items = new ArrayList<T>( initialList );
    }

    @Override
    public int size() {
        return this.items.size();
    }

    @Override
    public boolean isEmpty() {
        return this.items.isEmpty();
    }

    @Override
    public boolean contains( @Nullable Object o ) {
        return this.items.contains( o );
    }

    @NonNull
    @Override
    public Iterator<T> iterator() {
        return this.items.iterator();
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return this.items.toArray();
    }

    @NonNull
    @Override
    public <T1> T1[] toArray( @NonNull T1[] a ) {
        return this.items.toArray( a );
    }


    @Override
    public boolean containsAll( @NonNull Collection<?> c ) {
        return this.items.containsAll( c );
    }

    @Override
    public boolean addAll( @NonNull Collection<? extends T> c ) {
        int startIndex = this.size();
        int endIndex = startIndex + c.size();
        boolean added = this.items.addAll( c );
        if ( added ) {
            this.notifyItemRangeInserted( startIndex, endIndex );
        }
        return added;
    }

    @Override
    public boolean addAll( int index, @NonNull Collection<? extends T> c ) {
        int startIndex = index;
        int endIndex = startIndex + c.size();
        boolean added = this.items.addAll( index, c );
        if ( added ) {
            notifyItemRangeInserted( startIndex, endIndex );
        }
        return added;
    }

    @Override
    public boolean removeAll( @NonNull Collection<?> c ) {
        throw new UnsupportedOperationException( "List adapter not support remove all operation" );
    }

    @Override
    public boolean retainAll( @NonNull Collection<?> c ) {
        throw new UnsupportedOperationException( "List adapter not support retain all operation" );
    }

    @Override
    public T set( int index, T element ) {
        // removes item at specified index.
        T removedItem = this.items.remove( index );
        notifyItemRemoved( index );

        // inserts item at specified index.
        this.items.add( index, element );
        notifyItemInserted( index );

        return removedItem;
    }

    @Override
    public void add( int index, T element ) {
        this.items.add( index, element );
        notifyItemInserted( index );
    }

    @Override
    public T remove( int index ) {
        T removedItem = this.items.remove( index );
        notifyItemRemoved( index );
        return removedItem;
    }

    @Override
    public int indexOf( @Nullable Object o ) {
        return this.items.indexOf( o );
    }

    @Override
    public int lastIndexOf( @Nullable Object o ) {
        return this.items.lastIndexOf( o );
    }

    @NonNull
    @Override
    public ListIterator<T> listIterator() {
        return this.items.listIterator();
    }

    @NonNull
    @Override
    public ListIterator<T> listIterator( int index ) {
        return this.items.listIterator( index );
    }

    @NonNull
    @Override
    public List<T> subList( int fromIndex, int toIndex ) {
        return this.items.subList( fromIndex, toIndex );
    }

    @NonNull
    @Override
    @CallSuper
    public GenericViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType ) {
        this.context = parent.getContext();
        return this.createViewHolder( this.inflateView( parent, LayoutInflater.from( context ) ) );
    }

    /**
     * Adds an item interaction observer and returns his bounded index.
     *
     * @param onItemInteraction Added observer.
     * @return Added item interaction observer's id.
     */
    public int addObserver( OnItemInteraction onItemInteraction ) {
        int previousSize = getItemCount();
        this.observers.add( onItemInteraction );
        return previousSize;
    }


    /**
     * Removes observer bound with his id.
     *
     * @param observerId
     */
    public void removeObserver( int observerId ) {
        this.observers.remove( observerId );
    }

    /**
     * Adds an item in adapter.
     *
     * @param item Added item.
     */
    public boolean add( T item ) {
        boolean added = this.items.add( item );
        if ( added ) {
            this.notifyItemInserted( getItemCount() - 1 );
        }
        return added;
    }

    /**
     * Removes an item from adapter.
     *
     * @param item Removed item.
     */
    public boolean remove( @Nullable Object item ) {
        int position = this.items.indexOf( item );
        if ( position != ITEM_NOT_FOUND_INDEX ) {
            this.items.remove( position );
            this.notifyItemRemoved( position );
            return true;
        } else {
            return false;
        }
    }

    /**
     * Clears adapter.
     */
    public void clear() {
        int size = this.getItemCount();
        this.items.clear();
        this.notifyItemRangeRemoved( 0, size );
    }


    /**
     * Gets the first item stored in adapter.
     *
     * @return The first item in adapter.
     */
    public T getFirst() {
        return this.items.get( 0 );
    }

    /**
     * Gets the last item stored in adapter.
     *
     * @return The last item in adapter.
     */
    public T getLast() {
        return this.items.get( this.items.size() - 1 );
    }

    /**
     * Gets item stored at specified index.
     *
     * @param index Searched item's index.
     * @return
     */
    public T get( int index ) {
        return this.items.get( index );
    }


    @Override
    public void onBindViewHolder( @NonNull GenericViewHolder holder, int position ) {
        holder.setContext( context );

        // updates item
        T item = items.get( position );
        holder.updateViewHolder( item );


        holder.getItemView().setOnClickListener( v -> {
            notifyAllObservers( item );
        } );
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Inflates view.
     *
     * @param parent         Parent view group.
     * @param layoutInflater Layout inflater used to inflate view.
     * @return Inflated view.
     */
    public abstract View inflateView( ViewGroup parent, LayoutInflater layoutInflater );

    /**
     * Create view holder to hold item.
     *
     * @param view Not configured view.
     * @return Generic view holder used to wrap view.
     */
    public abstract GenericViewHolder createViewHolder( View view );

    /**
     * Sorts stored items in adapter using QuickSort algorithm.
     *
     * To compare generic items, a comparator which is able to compare two generic items
     * must be provided.
     *
     * A consequence of sorting items is that item's order might change.
     * To keep user interface up-to-date with new order, adapter is notify
     * that items are changed.
     *
     * @param comparator Comparator used to compare two generic items.
     */
    public void sortItems( Comparator<T> comparator ) {
        this.items = quickSort( this.items, comparator );
        notifyItemRangeChanged( 0, this.items.size() );
    }

    /**
     * QuickSort algorithm implemented for generic items.
     *
     * This algorithm is executed in time O(n * log(n)), using probabilistic approach.
     * To sort a list of item, a random element called a pivot is picked from list.
     * Then each element is compared with the pivot and put in a different set, depending of
     * that the value is lower or equals than pivot, or higher.
     * After that, left and right sets are stored recusively.
     * Finally, the initial sorted list is equals to sorted left set,
     * plus the pivot at the middle, plus sorted right set.
     *
     * @param items List of items to sort.
     * @param comparator Comparator to compare two generic items.
     * @param <T> Specifies item's type.
     *
     * @return Sorted items.
     */
    private static <T> List<T> quickSort( List<T> items, Comparator<T> comparator ) {
        // in a sort recursion, an empty or a singleton set is always sorted.
        int size = items.size();
        if ( size <= 1 ) {
            return items;
        }

        // select random pivot in list to exploit probabilistic approach
        Random random = new Random();
        int pivotIndex =  random.nextInt( items.size() );
        T pivot = items.get( pivotIndex );

        // quick sort put item in a left set when compared item is lower or equals than random pivot
        // otherwise, the compared item is put to a right set.
        // The pivot must be ignore from separation step
        List<T> leftSet = new ArrayList<>();
        List<T> rightSet = new ArrayList<>();
        for ( int comparedItemIndex = 0; comparedItemIndex < size; ++comparedItemIndex ) {
            if ( comparedItemIndex != pivotIndex ) {
                T comparedItem = items.get( comparedItemIndex );
                if ( 0 <= comparator.compare( pivot, comparedItem ) ) {
                    leftSet.add( comparedItem );
                } else {
                    rightSet.add( comparedItem );
                }
            }
        }

        // sort recursively left and right sets
        List<T> sortedLeftSet = quickSort( leftSet, comparator );
        List<T> sortedRightSet = quickSort( rightSet, comparator );

        // at the end, sorted values lower or equals than pivot are put at left of pivot
        // and values higher than pivot are put at right of pivot.
        List<T> sortedItems = new ArrayList<>( sortedLeftSet );
        sortedItems.add( pivot );
        sortedItems.addAll( sortedRightSet );
        return sortedItems;
    }

    /**
     * Notify all observers that's an item has been selected.
     *
     * @param item Selected item.
     */
    private void notifyAllObservers( T item ) {
        for ( OnItemInteraction onItemInteraction : this.observers ) {
            onItemInteraction.onItemInteraction( item );
        }
    }


}
