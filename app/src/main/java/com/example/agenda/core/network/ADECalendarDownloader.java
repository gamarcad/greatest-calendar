package com.example.agenda.core.network;

import android.os.AsyncTask;
import android.util.Log;

import com.example.agenda.activities.scholar_years.creation.DownloadLifeListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Async task used to download ADE Calendar from ADE calendar.
 */
public class ADECalendarDownloader extends
        AsyncTask< String, ADECalendarDownloadStatus, ADECalendarParser> {

    /**
     * Download request tag
     */
    private static final String DOWNLOAD_TAG = "DOWNLOAD_TAG";

    /**
     * ADE Calendar url
     */
    private String calendarURL;

    /**
     * Downloader listener to notify download cycle life.
     */
    private DownloadLifeListener<ADECalendarParser> lifeListener;

    public ADECalendarDownloader( String calendarURL, DownloadLifeListener lifeListener ) {
        this.calendarURL = calendarURL;
        this.lifeListener = lifeListener;
    }

    @Override
    protected void onPreExecute() {
        this.lifeListener.onDownloadStart();
    }

    @Override
    protected ADECalendarParser doInBackground( String... strings ) {
        publishProgress( ADECalendarDownloadStatus.IN_PROGRESS );

        // Gather events from url
        List< String > lines = new ArrayList<>();
        try {
            URL url = new URL( getCalendarURL() );
            URLConnection connection = url.openConnection();

            // reading stream from activity
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader( connection.getInputStream(), Charset.defaultCharset() ) );


            String line = null;
            while ( ( line = reader.readLine() ) != null ) {
                lines.add( line );
            }


        } catch ( MalformedURLException e ) {
            Log.w( DOWNLOAD_TAG,  "Malformed url: " + e.getMessage() );
            return null;

        } catch ( IOException e ) {
            Log.w( DOWNLOAD_TAG, "Input Ouput Exception: " + e.getMessage()  );
            return null;
        }

        publishProgress( ADECalendarDownloadStatus.POST_DOWNLOAD );

        try {
            return ADECalendarParser.parseCalendar( lines );
        } catch ( ParseException e ) {
            return null;
        }
    }

    @Override
    protected void onProgressUpdate( ADECalendarDownloadStatus... values ) {
        switch ( values[0] ) {
            case POST_DOWNLOAD:
                // notify listener that download is complete and post-processing in is process
                this.lifeListener.onPostDownload();
                break;
        }
    }

    @Override
    protected void onPostExecute( ADECalendarParser ADECalendarParser ) {
        if ( ADECalendarParser != null ) {
            this.lifeListener.onDownloadSuccess( ADECalendarParser );
        } else {
            this.lifeListener.onDownloadFailed();
        }
        this.lifeListener.onDownloadFinish();
    }

    private String getCalendarURL() {
        return this.calendarURL;
    }

}
