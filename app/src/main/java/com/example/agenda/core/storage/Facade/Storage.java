package com.example.agenda.core.storage.Facade;

import android.content.Context;

import com.example.agenda.core.storage.AppliDatabase;
import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;
import com.example.agenda.core.storage.Entity.ScholarYear;


import java.util.List;

/**
 * Facade which ease utilization of the database
 */
public class Storage implements StorageInterface {

    // Database
    private AppliDatabase db;

    // The maker of Storage get an instance of the database
    public Storage(Context context){ db = AppliDatabase.getInstance(context); }


    @Override
    public List<Event> getAllEvents() { return db.EventDao().getAllEvent(); }

    @Override
    public Event getEvent(long eventId) {
        return db.EventDao().getEvent(eventId);
    }

    @Override
    public Module getModule(String moduleName) { return db.ModuleDao().getModule(moduleName); }

    @Override
    public List<Module> getAllModules() {
        return db.ModuleDao().getAllModule();
    }

    @Override
    public ScholarYear getScholarYear(String name) { return db.ScholarYearDao().getScholarYear(name); }

    @Override
    public List<ScholarYear> getAllScholarYears() { return db.ScholarYearDao().getAllScholarYear(); }

    @Override
    public void insertEvent(Event event) {
        event.setId( db.EventDao().insert(event) );
    }

    @Override
    public void insertModule(Module module) {
        module.setId( db.ModuleDao().insert(module) );
    }

    @Override
    public void insertScholarYear(ScholarYear scholarYear) {
        scholarYear.setId( db.ScholarYearDao().insert(scholarYear) );
    }

    @Override
    public void updateEvent(Event event) { db.EventDao().update(event); }

    @Override
    public void updateModule(Module module) { db.ModuleDao().update(module); }

    @Override
    public void updateScholarYear(ScholarYear scholarYear) { db.ScholarYearDao().update(scholarYear); }

    @Override
    public void deleteEvent(Event event) { db.EventDao().delete(event.getId()); }

    @Override
    public void deleteModule(Module module) { db.ModuleDao().delete(module.getId()); }

    @Override
    public void deleteScholarYear(ScholarYear scholarYear) { db.ScholarYearDao().delete(scholarYear.getId()); }
}
