package com.example.agenda.core.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * GenericViewHolder wraps view initialization and management.
 *
 * @param <T> Item type.
 */
public abstract class GenericViewHolder< T > extends RecyclerView.ViewHolder {

    /**
     * View holder context.
     */
    private Context context;

    /**
     * view which contains GUI elements.
     */
    private View itemView;

    /**
     * Initialize view holder with GUI elements.
     *
     * @param itemView View which contains GUI elements.
     */
    public GenericViewHolder( @NonNull View itemView ) {
        super( itemView );
        this.itemView = itemView;
    }


    /**
     * Returns item view.
     *
     * @return Item view.
     */
    public View getItemView() {
        return this.itemView;
    }

    /**
     * Returns displaying context.
     *
     * @return Displaying context.
     */
    protected Context getContext() {
        return this.context;
    }

    /**
     * Update item view with provided item.
     *
     * @param item Provided item.
     */
    public abstract void updateViewHolder( T item );

    /**
     * Setting displaying context.
     *
     * @param context Displaying context.
     */
    protected void setContext( Context context ) {
        this.context = context;
    }

}
