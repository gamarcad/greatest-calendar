package com.example.agenda.core;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
/*
@Entity(tableName = "ProtoScholarYear")
public class ProtoScholarYear implements Serializable {

    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
    private static final long ONE_HOUR = 60 * 60 * 1000;


    @PrimaryKey
    @ColumnInfo(name = "id")
    private long id;

    //TODO Foreign key tag missing ?
    @ColumnInfo(name = "title")
    private String title;

    @Ignore
    private List<Module> moduleList;

    @ColumnInfo(name = "initialYear")
    private long initialYear;

    @ColumnInfo(name = "lastSynchronize")
    private String lastSynchronize;

    @ColumnInfo(name = "calendar")
    private String calendarURL;

    @Ignore
    public ProtoScholarYear() {
        this.moduleList = new ArrayList<>();
    }


    public ProtoScholarYear( long id, String title, long initialYear, String lastSynchronize, String calendarURL) {
        this.id = id;
        this.title = title;
        this.moduleList = new ArrayList<>();
        this.initialYear = initialYear;
        this.lastSynchronize = lastSynchronize;
        this.calendarURL = calendarURL;
    }

    /**
     * Adds events in the scholar year.
     *
     * @param events Collections of events.
     *
    public void addAllEvents( Iterable<Event> events ) {
        for ( Event event : events ) {
            addEvent( event );
        }
    }

    /**
     * Adds an event to the scholar years.
     *
     * @param event Collection event.
     *
    public void addEvent( Event event ) {
        String summary = event.getSummary();

        // looking for module called summary
        for ( Module module : this.moduleList ) {
            if ( summary.equals( module.getSummary() ) ) {
               // module.addEvent( event );
                return;
            }
        }

        // module is not present
        this.moduleList.add( createModuleFromEvent( event ) );
    }

    @Override
    public boolean equals( @Nullable Object obj ) {
        if ( obj == null || !( obj instanceof ProtoScholarYear ) ) {
            return false;
        }

        return (( ProtoScholarYear )obj).getTitle().equals( getTitle() );
    }

    /**
     * Synchronize all events in the scholar year with all these in
     * events container.
     *
     * @param eventsContainer Events defined as new reference for scholar year.
     *
    public void synchronizeEvents( EventsContainer eventsContainer ) {
        for ( Module module : this.moduleList ) {
            module.updateEvents( eventsContainer );
        }
    }

    /**
     * Return {@code true} if and only if scholar years is synchronized.
     *
     * @return {@code true} if and only if scholar years is synchronized, {@code false} otherwise.
     *
    public boolean isSynchronized(){
        if ( this.getLastSynchronizeDate() == null ) {
            return false;
        }

        Date now = new Date();
        Date limitTime = this.getDateIn6Hours( getLastSynchronizeDate());
        return now.compareTo( limitTime ) < 0;
    }

    public void notifyAsSynchronized() {
        setLastSynchronize(new Date());
    }



    private Module createModuleFromEvent( Event event ) {
        Module module = new Module( event.getSummary() );
        module.addEvent( event );
        return module;
    }

    private Date getDateIn6Hours( Date date ) {
        return new Date( date.getTime() + 6 * ONE_HOUR );
    }

    //**************************
    //*   GETTER AND SETTER    *
    //**************************

    public long getId() { return id; }

    public String getTitle() {
        return title;
    }

    public List<Module> getModuleList() {
        return moduleList;
    }

    public long getInitialYear() {
        return initialYear;
    }

    public String getCalendarURL() {
        return calendarURL;
    }

    public Date getLastSynchronizeDate() {
        try {
            return new SimpleDateFormat(DATE_FORMAT, Locale.FRANCE).parse(lastSynchronize);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public String getLastSynchronize(){
        return lastSynchronize;
    }

    public void setLastSynchronize(String lastSynchronize) {
        this.lastSynchronize = lastSynchronize;
    }

    public void setId(long id) { this.id = id; }

    public void setTitle( String title ) {
        this.title = title;
    }

    public void setModuleList(List<Module> moduleList) { this.moduleList = moduleList; }

    public void setInitialYear( long initialYear ) {
        this.initialYear = initialYear;
    }

    public void setCalendarURL( String calendarURL ) {
        this.calendarURL = calendarURL;
    }

    public void setLastSynchronize(Date lastSynchronize) {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT,Locale.FRANCE);
        this.lastSynchronize = dateFormat.format(lastSynchronize);
    }


    @Override
    public String toString() {
        return "ProtoScholarYear{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", moduleList=" + moduleList +
                ", initialYear=" + initialYear +
                ", lastSynchronize='" + lastSynchronize + '\'' +
                ", calendarURL='" + calendarURL + '\'' +
                '}';
    }
}*/
