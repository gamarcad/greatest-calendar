package com.example.agenda.core.buffer;

import android.os.AsyncTask;
import android.util.Log;

import com.example.agenda.core.storage.Facade.StorageInterface;

import java.util.logging.Logger;

/**
 * Load buffer with entities stored in database.
 */
public class BufferLoader extends AsyncTask<Void, Void, Void> {
    private OnDatabaseInteraction onDatabaseInteraction;
    private StorageInterface storage;
    private Buffer buffer;

    public BufferLoader( StorageInterface storage ) {
        this.buffer = Buffer.getInstance();
        this.storage = storage;
    }

    public void setOnDatabaseInteraction( OnDatabaseInteraction onDatabaseInteraction ) {
        this.onDatabaseInteraction = onDatabaseInteraction;
    }

    @Override
    protected void onPreExecute() {
        if ( onDatabaseInteraction != null ) {
            onDatabaseInteraction.onStart();
        }
    }

    @Override
    protected Void doInBackground( Void... voids ) {
        // loading scholar years
        buffer.setScholarYears( storage.getAllScholarYears() );

        // loading modules
        buffer.setModules( storage.getAllModules() );

        // loading events
        buffer.setEvents( storage.getAllEvents() );

        return null;
    }

    @Override
    protected void onPostExecute( Void aVoid ) {
        if ( onDatabaseInteraction != null ) {
            onDatabaseInteraction.onFinish();
        }
    }
}
