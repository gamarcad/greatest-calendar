package com.example.agenda.core.storage.Entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class ScholarYearAndAllModules {

    @Embedded
    public ScholarYear scholarYear;

    @Relation(parentColumn = "id" , entityColumn = "scholar_id", entity = Module.class)
    public List< Module > moduleList;

    public ScholarYearAndAllModules(){

    }

    private Module createModuleFromEvent( Event event ) {
        Module module = new Module( event.getSummary() );
        ModuleAndAllEvents moduleAndAllEvents = null;
        moduleAndAllEvents.module = module;
        moduleAndAllEvents.addEvent( event );
        return module;
    }

    /**
     * Adds an event to the scholar years.
     *
     * @param event Collection event.
     */
    public void addEvent( Event event ) {
        String summary = event.getSummary();

        // looking for module called summary
        for ( Module module : this.moduleList ) {
            if ( summary.equals( module.getSummary() ) ) {
                ModuleAndAllEvents moduleAndAllEvents = null;
                moduleAndAllEvents.module = module;
                moduleAndAllEvents.addEvent( event );
                return;
            }
        }


        // module is not present
        this.moduleList.add( createModuleFromEvent( event ) );
    }

    /**
     * Adds events in the scholar year.
     *
     * @param events Collections of events.
     */
    public void addAllEvents( Iterable< Event > events ) {
        for ( Event event : events ) {
            addEvent( event );
        }
    }



    /**
     * Synchronize all events in the scholar year with all these in
     * events container.
     *
     * @param eventsContainer Events defined as new reference for scholar year.
     */
    public void synchronizeEvents( List<Event> eventsContainer ) {
        for ( Module module : this.moduleList ) {
            ModuleAndAllEvents moduleAndAllEvents = null;
            moduleAndAllEvents.module = module;
            moduleAndAllEvents.updateEvents( eventsContainer );
        }
    }
}

