package com.example.agenda.core.storage.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "ScholarYear")
public class ScholarYear implements Serializable {
    public static final String DATE_FORMAT = "HH:mm dd/MM/yyyy";
    private static final long ONE_HOUR = 60 * 60 * 1000;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "initialYear")
    private long initialYear;

    @ColumnInfo(name = "lastSynchronize")
    @TypeConverters({Converters.class})
    private Date lastSynchronize;

    @ColumnInfo(name = "calendar")
    private String calendarURL;

    public ScholarYear() {

    }


    public ScholarYear(long id, String title, long initialYear, Date lastSynchronize, String calendarURL) {
        this.id = id;
        this.title = title;
        this.initialYear = initialYear;
        this.lastSynchronize = lastSynchronize;
        this.calendarURL = calendarURL;
    }




    public boolean isSynchronized(){
        if ( this.getLastSynchronize() == null ) {
            return false;
        }

        Date now = new Date();
        Date limitTime = this.getDateIn6Hours( getLastSynchronize());
        return now.compareTo( limitTime ) < 0;
    }

    public void notifyAsSynchronized() {
        setLastSynchronize(new Date());
    }


    private Date getDateIn6Hours( Date date ) {
        return new Date( date.getTime() + 6 * ONE_HOUR );
    }

    //**************************
    //*   GETTER AND SETTER    *
    //**************************

    public long getId() { return id; }

    public String getTitle() {
        return title;
    }

    public long getInitialYear() {
        return initialYear;
    }

    public String getCalendarURL() {
        return calendarURL;
    }

    public Date getLastSynchronize(){
        return lastSynchronize;
    }


    public void setId(long id) { this.id = id; }

    public void setTitle( String title ) {
        this.title = title;
    }

    public void setInitialYear( long initialYear ) {
        this.initialYear = initialYear;
    }

    public void setCalendarURL( String calendarURL ) {
        this.calendarURL = calendarURL;
    }

    public void setLastSynchronize(Date lastSynchronize) { this.lastSynchronize = lastSynchronize; }

    @Override
    public String toString() {
        return "ScholarYear{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", initialYear=" + initialYear +
                ", lastSynchronize=" + lastSynchronize +
                ", calendarURL='" + calendarURL + '\'' +
                '}';
    }
}
