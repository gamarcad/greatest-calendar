package com.example.agenda.activities.calendar.display;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.agenda.R;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.storage.Entity.Event;

class EventCalendarViewHolder extends GenericViewHolder<Event> {

    private TextView location;

    private TextView start_end;

    private TextView summary;

    private TextView description;

    EventCalendarViewHolder(@NonNull View itemView) {
        super(itemView);
        this.location = itemView.findViewById(R.id.location);
        this.start_end = itemView.findViewById(R.id.event_start_end);
        this.summary = itemView.findViewById(R.id.summary);
        this.description = itemView.findViewById(R.id.description);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void updateViewHolder(Event item) {

        String start = (String) DateFormat.format("HH:mm",item.getStartAt());
        String end = (String) DateFormat.format("HH:mm",item.getEndAt());
        String date = String.format(getContext().getResources().getString(R.string.duration_event),start,end);
        this.start_end.setText(date);

        this.location.setText(item.getLocation());
        this.summary.setText(item.getSummary());
        this.description.setText(item.getDescription());
    }
}
