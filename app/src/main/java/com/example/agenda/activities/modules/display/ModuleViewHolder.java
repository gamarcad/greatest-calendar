package com.example.agenda.activities.modules.display;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.agenda.R;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.storage.Entity.Module;

class ModuleViewHolder extends GenericViewHolder<Module> {


    private TextView summary;

    ModuleViewHolder(@NonNull View itemView) {
        super(itemView);
        this.summary = itemView.findViewById(R.id.module_summary);
    }


    public void updateViewHolder(Module module) {
        this.summary.setText(module.getSummary());
    }
}
