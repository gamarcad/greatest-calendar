package com.example.agenda.activities.calendar.display;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.agenda.R;
import com.example.agenda.core.adapter.GenericViewHolder;
import com.example.agenda.core.adapter.ListAdapter;
import com.example.agenda.core.storage.Entity.Event;

import java.util.List;

class EventListAdapter extends ListAdapter<Event> {

    EventListAdapter(List<Event> events) {
        super(events);
    }


    @Override
    public GenericViewHolder createViewHolder(View view) {
        return new EventCalendarViewHolder(view);
    }


    @Override
    public View inflateView(ViewGroup parent, LayoutInflater layoutInflater) {
        return layoutInflater.inflate(R.layout.fragment_event_calendar_list,parent,false);
    }


}
