package com.example.agenda.activities.events.display;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.example.agenda.R;
import com.example.agenda.activities.modules.display.ModuleDisplayActivity;
import com.example.agenda.activities.scholar_years.display.OnItemInteraction;
import com.example.agenda.core.buffer.Buffer;
import com.example.agenda.core.storage.Entity.Event;
import com.example.agenda.core.storage.Entity.Module;
import android.util.Log;
import android.view.Menu;



public class EventDisplayActivity extends AppCompatActivity implements OnItemInteraction<Event> {

    private long module_id;

    private EventFragment eventFragment;

    private EventDetailsFragment eventDetailsFragment;


    public EventDisplayActivity(){ }

    public long getModule_id() {
        return module_id;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent module = getIntent();

        if(module != null){
            module_id = module.getLongExtra("module_id",-1);
            Log.i("NORMAL RUNNING", "Information found in Intent");
        }else{
            Log.i("ANORMAL RUNNING","EMPTY INTENT");
        }



        Log.i("SET CONTENT VIEW ", " Content view :"+R.layout.activity_module_details);
        setContentView(R.layout.activity_module_details);
        startToolbar();
        setTitle(Buffer.getInstance().getModulebyId(module_id).getTitle());


        Log.i("INITIALISATION ", " Initialisation of fragments ");
        eventFragment = (EventFragment)getSupportFragmentManager().findFragmentById(R.id.event_frag);
        eventDetailsFragment = (EventDetailsFragment)getSupportFragmentManager().findFragmentById(R.id.event_detail_frag);


    }

    private void startToolbar() {
        Toolbar toolbar = findViewById( R.id.tool_bar);
        this.setSupportActionBar( toolbar );

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.menu_event, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        int id = item.getItemId();

        if ( id == R.id.menu_event_back) {
            goToModuleActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToModuleActivity(){
        Log.i("GOTO MODULE","Return in module display activity");
        finish();
    }

    @Override
    public void onItemInteraction(Event event) {
        Log.i( "SCHOLAR_YEAR_INTERACT", "onItemInteraction: Starting "
                + event.getSummary() );
        assert eventDetailsFragment != null;
        eventDetailsFragment.update(event);
    }


}
