package com.example.agenda.activities.events.display;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.agenda.R;
import com.example.agenda.activities.scholar_years.display.OnItemInteraction;
import com.example.agenda.core.buffer.Buffer;
import com.example.agenda.core.storage.Entity.Event;

import java.util.List;

public class EventFragment extends Fragment {

    private OnItemInteraction listener;

    private List<Event> eventList;

    private EventsAdapter eventsAdapter;

    private long module_id;


    public EventFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventDisplayActivity ed = (EventDisplayActivity)getActivity();
        assert ed != null;
        module_id = ed.getModule_id();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            Log.i("CONTEXT EVENT LIST",context.toString());
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            eventList = Buffer.getInstance().getEventsById(module_id);

            Log.i("EVENT LIST" , module_id+" "+eventList.toString());

            if(eventsAdapter == null){
                Log.i("CREATE ADAPTER",module_id+" ");
                eventsAdapter = new EventsAdapter(eventList);
                eventsAdapter.addObserver(listener);
                recyclerView.setAdapter(eventsAdapter);
            }

        }
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnItemInteraction) {
            listener = (OnItemInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


}
