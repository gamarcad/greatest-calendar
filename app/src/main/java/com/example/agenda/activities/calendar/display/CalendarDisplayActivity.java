package com.example.agenda.activities.calendar.display;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.agenda.R;
import com.example.agenda.activities.scholar_years.display.ScholarYearsDisplayActivity;
import com.example.agenda.core.storage.Entity.Event;

import java.util.ArrayList;
import java.util.List;

public class CalendarDisplayActivity extends AppCompatActivity {

    private CalendarFragment calendarFragment;

    private EventListFragment eventListFragment;

    private List<Event> events;

    public CalendarDisplayActivity(){
        events = new ArrayList<Event>();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_calendar);
        startToolbar();
        setTitle(R.string.title_calendar);
        calendarFragment = (CalendarFragment) getSupportFragmentManager().findFragmentById(R.id.calendar_frag);
        eventListFragment = (EventListFragment) getSupportFragmentManager().findFragmentById(R.id.event_list_calendar_frag);


    }

    private void startToolbar() {
        Toolbar toolbar = findViewById( R.id.tool_bar);
        this.setSupportActionBar( toolbar );

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.menu_event, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        int id = item.getItemId();

        if ( id == R.id.menu_event_back) {
            goToMainActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToMainActivity() {
        Log.i("GOTO MAIN","Return in main activity");
        startActivity(new Intent(this, ScholarYearsDisplayActivity.class));
    }


}
